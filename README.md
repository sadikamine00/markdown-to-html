# Markdown to GitLab style web

> Thanks Felix Krause, this is the clone of [Markdown to GitHub style web](https://github.com/KrauseFx/markdown-to-html-github-style)

## Background

If you have a little side project, often you might want a simple landing page. The GitLab `README` rendering is really beautifully done: clean, simple and modern.

Using your GitLab `README` as the main landing point works great for open source projects, where your visitors are developers and are familiar with GitLab, as well as you have all the text right where the code, the Issues and MRs are. But for some projects this isn't ideal.

## Solution

A simple script that converts a markdown (`.md`) file to a single HTML file, that includes all the HTML and CSS code inline, so you can host it anywhere.

There is no need to use this script if you already convert your markdown file to HTML, you can directly use the [stylesheet](https://gitlab.com/yoginth/markdown-to-html/blob/master/style.css) of this repo.

## How it works

This project doesn't actually use the GitLab stylesheet, it's far too complex, and has legal implications.

Instead this project does 2 things:

- Convert the Markdown to HTML using [showdown](https://github.com/showdownjs/showdown), the most popular JS markdown parser.
- Inject the GitLab-like CSS code at the bottom of the page

Resulting you get an HTML file that contains everything needed, so you can host the page on GitLab pages, AWS S3, Google Cloud Storage or anywhere else.

- Check out [the original markdown](https://gitlab.com/yoginth/markdown-to-html/blob/master/README.md) file of this `README`
- Check out the [raw generated HTML code](https://gitlab.com/yoginth/markdown-to-html/blob/master/index.html) generated based on this markdown file on
- Check out the [GitLab rendered README](https://gitlab.com/yoginth/markdown-to-html)
- Check out the [README rendered by this project](https://md.yoginth.com)

## Usage

```
node convert.js MyPageTitle
```

## Open tasks

Check out the [open issues](https://gitlab.com/yoginth/markdown-to-html/issues), in particular code blocks currently don't support syntax highlighting, however that's something that's rather easy to add.

## Playground to test

```
{
  testcode: 1
}
```

- Bullet list item 1
- Bullet list item 2
    - Bullet list item 2.1
    - Bullet list item 2.2

---

1. Numbered list item 1
1. Numbered list item 2

Inline `code` comments are `100`

> Quoted texts are more gray and look differently

**Bold text** is **bold** and [inline links](https://yoginth.com) work as well.

# Header 1
## Header 2
### Header 3
#### Header 4
##### Header 5
###### Header 6

<table>
  <tr>
    <td>
      <img src="https://raw.githubusercontent.com/KrauseFx/markdown-to-html-github-style/master/demo/screenshot1_framed.jpg">
    </td>
    <td>
      <img src="https://raw.githubusercontent.com/KrauseFx/markdown-to-html-github-style/master/demo/screenshot2_framed.jpg">
    </td>
    <td>
      <img src="https://raw.githubusercontent.com/KrauseFx/markdown-to-html-github-style/master/demo/screenshot3_framed.jpg">
    </td>
  </tr>
</table>

Normal text content again, lorem ipsum

<table>
  <tr>
    <th>
      Text 1
    </th>
    <th>
      Text 2
    </th>
    <th>
      Text 3
    </th>
  </tr>
  <tr>
    <td>
      Text 1
    </td>
    <td>
      Text 2
    </td>
    <td>
      Text 3
    </td>
  </tr>
  <tr>
    <td>
      Text 1
    </td>
    <td>
      Text 2
    </td>
    <td>
      Text 3
    </td>
  </tr>
</table>

<h3 style="text-align: center; font-size: 35px; border: none">
  <a href="https://t.me/WalkWithFriendsBot" target="_blank" style="text-decoration: none;">
    🔰 Raw HTML code for custom style 🔰
  </a>
</h3>
